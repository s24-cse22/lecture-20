#ifndef DATA_H
#define DATA_H

#include <stdexcept>

int findMax(int values[], int size){

    if (size < 1){
        throw std::logic_error("Invalid array size");
    }

    int answer = values[0];

    for (int i = 0; i < size; i++){
        if (values[i] > answer){
            answer = values[i];
        }
    }

    return answer;
}

int countNegative(int values[], int size){
    if (size < 1){
        throw std::logic_error("Invalid array size");
    }
    int c = 0;
    for(int i = 0; i < size; i++){
        if (values[i] < 0){
            c++;
        }
    }
    return c;
}

int addUp(int values[], int size){
    if (size < 1){
        throw std::logic_error("Invalid array size");
    }
    int total = 0;
    for(int i = 0; i < size; i++){
        total = total + values[i];
    }
    return total;
}

bool search(int values[], int size, int target){
    for (int i = 0; i < size; i++){
        if (values[i] == target){
            return true;
        }
        // else{
        //     return false;
        // }
    }
    return false;
}

#endif